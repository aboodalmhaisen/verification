package com.example.abood.verification

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import kotlinx.android.synthetic.main.activity_signup.*

class SignUpActivity : AppCompatActivity() {
var mAuth:FirebaseAuth= FirebaseAuth.getInstance()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)


        mSubmit.setOnClickListener {


            mAuth.createUserWithEmailAndPassword(mEmaill.text.toString(), mPass.text.toString()).addOnCompleteListener(
                    this, {
                    if (it.isSuccessful) {
                        emailVerification()
                        var user: FirebaseUser? = mAuth.getCurrentUser();
                        var intent: Intent = Intent(this, VerificationActivity::class.java)
                        startActivity(intent)
                    }else{

                    }

                }).addOnFailureListener({

                    Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                })
            }
        }
    fun validation() {
        var fullName = mName.text.toString()
        var emailAddress = mEmaill.text.toString()
        var password = mPass.text.toString()
        var confirm=mConfirmPassword.text.toString()
        var validate:Boolean = true

        if (fullName.isEmpty()) {
            mName.error = "Please enter your name"
            validate = false        }
        if (password.isEmpty()) {
            mPass.error = "Please enter your password"
            validate = false
        }
        if (emailAddress.isEmpty()) {
            mEmaill.error = "Please enter your email"
            validate = false
        }
        if(confirm.isEmpty())
            mConfirmPassword.error="Please Confirm Your Password"

        if (!password.equals(confirm)){
            mConfirmPassword.error="Password Doesn't Match" }
        if (validate){
            var intent=Intent( this,VerificationActivity::class.java)
            startActivity(intent)
        }


    }
private fun emailVerification(){
    val user=FirebaseAuth.getInstance().currentUser
    user?.sendEmailVerification()?.addOnCompleteListener {
        Toast.makeText(this,"verification Link sent to your Email Please Check your Email ",Toast.LENGTH_SHORT).show()
    }

}
}






